import './App.css';
import React from "react";

import Balance from "./Components/Balance";
import ListExpenses from "./Components/ListExpenses";
import IncomeExpenese from "./Components/IncomeExpenses";
import Transaction from "./Components/Transaction";
import {GlobalProvider} from "./Context/GlobalContext";


function App() {
    return (
        <GlobalProvider>
            <div className="App">
                <h2>Expense Tracker</h2>
                <div className="container">
                    <Balance/>
                    <IncomeExpenese/>
                    <ListExpenses/>
                    <Transaction/>
                </div>
            </div>
        </GlobalProvider>
    );
}

export default App;
