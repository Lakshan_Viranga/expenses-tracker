import React,{useContext} from 'react'
import {TransactionContext} from "../Context/GlobalContext";

const IncomeExpenese=()=>{

    const {transactions}= useContext(TransactionContext)

    const amount = transactions.map(transactions=>transactions.amount);

    const income = amount.filter(item=>item>0).reduce((acc,item)=>acc+=item,0).toFixed(2)

    const expense = amount.filter(item=>item<0).reduce((acc,item)=>acc+=item,0).toFixed(2)

    return(
        <div className="inc-exp-container">
            <div>
                <h4>Income</h4>
                <p id="money-plus" className="money plus">LKR {income}</p>
            </div>
            <div>
                <h4>Expense</h4>
                <p id="money-minus" className="money minus">LKR {expense}</p>
            </div>
        </div>
    )
}

export default IncomeExpenese