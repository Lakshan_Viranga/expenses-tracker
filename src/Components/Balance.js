import React, {useContext} from 'react'
import {TransactionContext} from "../Context/GlobalContext";


const Balance = () => {

    const {transactions} = useContext(TransactionContext);

    const amount = transactions.map(transactions=>transactions.amount);

    const total = amount.reduce((acc,item)=>(acc+=item),0).toFixed(2)

    return (
        <>
            <h4>Your Balance</h4>
            <h1 id="balance">LKR {total}</h1>
        </>
    )
}

export default Balance