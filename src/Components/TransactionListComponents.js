import React,{useContext} from "react";
import {TransactionContext} from "../Context/GlobalContext";


const TransactionListComponents=({transactions})=>{

    const sign = transactions.amount<0 ? '-':'+'

    const {deleteTransaction} = useContext(TransactionContext)

    return(
        <li className={transactions.amount<0? 'minus':'plus'}>
            {transactions.text}<span>{sign}LKR{Math.abs(transactions.amount)}</span><button className="delete-btn" onClick={()=>deleteTransaction(transactions.id)
        }>x</button>
        </li>
    )
}

export default TransactionListComponents