import React,{useState,useContext} from 'react'
import {TransactionContext} from "../Context/GlobalContext";


const Transaction=()=>{

    const [text,setText] = useState("");
    const [amount, setAmount] = useState(0);

    const {addTransaction} = useContext(TransactionContext)

    const InputTextHandler=(e)=>{
        setText(e.target.value)
    }

    const InputAmountHandler=(e)=>{
        setAmount(e.target.value)
    }

    const onsubmit =(e)=>{
        e.preventDefault()

        const newTransaction = {
            id:Math.floor(Math.random()*1000000),
            text,
            amount:+amount
        }

        addTransaction(newTransaction)
    }

    return(
        <>
            <h3>Add new transaction</h3>
            <form id="form" onSubmit={onsubmit}>
                <div className="form-control">
                    <label htmlFor="text">Text</label>
                    <input type="text" id="text"  value={text} onChange={InputTextHandler} placeholder="Enter text..."/>
                </div>
                <div className="form-control">
                    <label htmlFor="amount">Amount <br/>
                        (negative - expense, positive - income)
                    </label>
                    <input type="number" id="amount" value={amount} onChange={InputAmountHandler} placeholder="Enter amount..."/>
                </div>
                <button className="btn">Add transaction</button>
            </form>
        </>
    )
}

export default Transaction