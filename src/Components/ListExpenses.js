import  React,{useContext} from 'react'
import {TransactionContext} from "../Context/GlobalContext";

import TransactionListComponents from "./TransactionListComponents";

const ListExpenses =()=>{

    const {transactions} = useContext(TransactionContext);

    return(
        <>
            <h3>History</h3>
            <ul id="list" className="list">
                {transactions.map(transactions=>(<TransactionListComponents key={transactions.id} transactions={transactions}/>))}
            </ul>
        </>
    )
}

export default ListExpenses