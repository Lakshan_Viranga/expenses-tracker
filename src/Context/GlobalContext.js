import React,{createContext,useReducer} from 'react'
import AppReducer from "./AppReducer";

const initialState = {
    transactions:[]
}
//Create global context
export const TransactionContext = createContext(initialState);

//Create context provider
export const GlobalProvider =({children})=>{
    const [state, dispatch] = useReducer(AppReducer,initialState);

    function deleteTransaction(id){
        dispatch({
            type:"DELETE",
            payload:id
        })
    }

    function addTransaction(transactions){
        dispatch({
            type:"ADD",
            payload:transactions
        })
    }

    return(
        <TransactionContext.Provider value={{transactions: state.transactions,
            deleteTransaction,
            addTransaction
        }}>
            {children}
        </TransactionContext.Provider>
    )
}